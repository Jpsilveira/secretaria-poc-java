package br.com.jpsilveira.secretaria.controller;

import br.com.jpsilveira.secretaria.model.Aluno;
import br.com.jpsilveira.secretaria.repository.AlunoRepository;
import br.com.jpsilveira.secretaria.repository.DisciplinaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class AlunoController {

    private AlunoRepository alunoRepository;
    private DisciplinaRepository disciplinaRepository;

    @Autowired
    public AlunoController(AlunoRepository alunoRepository, DisciplinaRepository disciplinaRepository) {
        this.alunoRepository = alunoRepository;
        this.disciplinaRepository = disciplinaRepository;
    }

    @GetMapping("/aluno/")
    public String listAlunos(Model model) {
        Iterable<Aluno> alunos = alunoRepository.findAll();
        model.addAttribute("alunos", alunos);
        model.addAttribute("template", "aluno");
        return "index";
    }

    @GetMapping("/aluno/{matricula}")
    public String listAlunoDisciplinas(@PathVariable("matricula") int matricula, Model model) {
        Aluno aluno = alunoRepository.findById(matricula)
                .orElseThrow(() -> new IllegalArgumentException("Matrícula inválida:" + matricula));

        model.addAttribute("disciplinas", aluno.getDisciplinas());
        model.addAttribute("template", "disciplina");
        return "index";
    }

    @GetMapping("/aluno/edit/{matricula}")
    public String editAluno(@PathVariable("matricula") int matricula, Model model) {
        Aluno aluno = alunoRepository.findById(matricula)
                .orElseThrow(() -> new IllegalArgumentException("Matrícula inválida:" + matricula));
        model.addAttribute("aluno", aluno);
        model.addAttribute("template", "update-aluno");
        return "index";
    }

    @PostMapping("/aluno/update/{matricula}")
    public String updateAluno(@PathVariable("matricula") int matricula, @Valid Aluno aluno,
                              BindingResult result, Model model) {
        if (result.hasErrors()) {
            aluno.setMatricula(matricula);
            model.addAttribute("template", "update-aluno");
            return "index";
        }

        Aluno alunoDisciplinas = alunoRepository.findById(matricula)
                .orElseThrow(() -> new IllegalArgumentException("Matrícula inválida:" + matricula));
        alunoDisciplinas.setNome(aluno.getNome());
        alunoRepository.save(alunoDisciplinas);

        model.addAttribute("alunos", alunoRepository.findAll());
        model.addAttribute("template", "aluno");
        return "index";
    }
}
