package br.com.jpsilveira.secretaria.controller;

import br.com.jpsilveira.secretaria.model.Disciplina;
import br.com.jpsilveira.secretaria.repository.DisciplinaRepository;
import br.com.jpsilveira.secretaria.repository.ProfessorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class DisciplinaController {

    private DisciplinaRepository disciplinaRepository;
    private ProfessorRepository professorRepository;

    @Autowired
    public DisciplinaController(DisciplinaRepository disciplinaRepository, ProfessorRepository professorRepository) {
        this.disciplinaRepository = disciplinaRepository;
        this.professorRepository = professorRepository;
    }

    @GetMapping("/disciplina/")
    public String listDisciplinas(Model model) {
        Iterable<Disciplina> disciplinas = disciplinaRepository.findAll();
        model.addAttribute("disciplinas", disciplinas);
        model.addAttribute("template", "disciplina");
        return "index";
    }

    @GetMapping("/disciplina/{codigo}")
    public String listDisciplinaAlunos(@PathVariable("codigo") int codigo, Model model) {
        Disciplina disciplina = disciplinaRepository.findById(codigo)
                .orElseThrow(() -> new IllegalArgumentException("Código inválido:" + codigo));
        model.addAttribute("alunos", disciplina.getAlunos());
        model.addAttribute("template", "aluno");
        return "index";
    }

    @GetMapping("/disciplina/edit/{codigo}")
    public String editDisciplina(@PathVariable("codigo") int codigo, Model model) {
        Disciplina disciplina = disciplinaRepository.findById(codigo)
                .orElseThrow(() -> new IllegalArgumentException("Código inválido:" + codigo));
        model.addAttribute("disciplina", disciplina);
        model.addAttribute("professores", professorRepository.findAll());
        model.addAttribute("template", "update-disciplina");
        return "index";
    }

    @PostMapping("/disciplina/update/{codigo}")
    public String updateDisciplina(@PathVariable("codigo") int codigo, @Valid Disciplina disciplina,
                                   BindingResult result, Model model) {
        if (result.hasErrors()) {
            disciplina.setCodigo(codigo);
            model.addAttribute("template", "update-disciplina");
            return "index";
        }

        disciplinaRepository.save(disciplina);
        model.addAttribute("disciplinas", disciplinaRepository.findAll());
        model.addAttribute("template", "disciplina");
        return "index";
    }
}
