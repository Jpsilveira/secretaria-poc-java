package br.com.jpsilveira.secretaria.controller;

import br.com.jpsilveira.secretaria.model.Aluno;
import br.com.jpsilveira.secretaria.model.Disciplina;
import br.com.jpsilveira.secretaria.model.Pessoa;
import br.com.jpsilveira.secretaria.model.Professor;
import br.com.jpsilveira.secretaria.repository.AlunoRepository;
import br.com.jpsilveira.secretaria.repository.DisciplinaRepository;
import br.com.jpsilveira.secretaria.repository.PessoaRepository;
import br.com.jpsilveira.secretaria.repository.ProfessorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Set;

@Controller
public class FileController {

    private AlunoRepository alunoRepository;
    private DisciplinaRepository disciplinaRepository;
    private PessoaRepository pessoaRepository;
    private ProfessorRepository professorRepository;

    @Autowired
    public FileController(AlunoRepository alunoRepository, DisciplinaRepository disciplinaRepository, PessoaRepository pessoaRepository, ProfessorRepository professorRepository) {
        this.alunoRepository = alunoRepository;
        this.disciplinaRepository = disciplinaRepository;
        this.pessoaRepository = pessoaRepository;
        this.professorRepository = professorRepository;
    }

    @GetMapping("/upload")
    public String upload(Model model) {
        model.addAttribute("template", "upload");
        return "index";
    }

    @PostMapping("/upload-file")
    public String uploadFile(@RequestParam("file") MultipartFile file, RedirectAttributes redirectAttributes) {

        clearDB();

        try {
            BufferedReader br;
            InputStream is = file.getInputStream();
            br = new BufferedReader(new InputStreamReader(is));

            int disciplinaCount = Integer.parseInt(br.readLine());
            for (int disciplinaItem = 0; disciplinaItem < disciplinaCount; disciplinaItem++) {

                String disciplinaLine = br.readLine();
                String professorLine = br.readLine();

                Professor professor = saveProfessor(professorLine);
                Disciplina disciplina = saveDisciplina(disciplinaLine, professor);

                int alunoCount = Integer.parseInt(br.readLine());
                for (int alunoItem = 0; alunoItem < alunoCount; alunoItem++) {
                    String alunoLine = br.readLine();
                    saveAluno(alunoLine, disciplina);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return "redirect:/";
    }


    @GetMapping("/download")
    public String download(Model model) {
        model.addAttribute("template", "download");
        return "index";
    }

    @RequestMapping(value = "/download-file", method = RequestMethod.GET)
    public String downloadFile(RedirectAttributes redirectAttributes, Model model) {

        List<Disciplina> disciplinas = disciplinaRepository.findAll(Sort.by("nome"));

        String file = "";
        for (Disciplina disciplina : disciplinas) {
            file += disciplina.getNome() + " " + disciplina.getProfessor().getNome() + System.lineSeparator();
        }

        List<Pessoa> pessoas = pessoaRepository.findAll(Sort.by("matricula"));

        for (Pessoa pessoa : pessoas) {
            file += pessoa.getNome() + " " + pessoa.getDisciplinas().size() + System.lineSeparator();
        }

        model.addAttribute("file", file);
        return "saida";

    }

    private void clearDB() {
        try {

            alunoRepository.deleteAll();
            disciplinaRepository.deleteAll();
            professorRepository.deleteAll();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Professor saveProfessor(String professorLine) {
        try {
            int professorMatricula = Integer.parseInt(professorLine.split(" ")[0]);
            Professor professor = professorRepository.findById(professorMatricula)
                    .orElse(new Professor());
            professor.setMatricula(Integer.parseInt(professorLine.split(" ")[0]));
            professor.setNome(professorLine.split(" ")[1]);
            professorRepository.save(professor);
            return professor;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private Disciplina saveDisciplina(String disciplinaLine, Professor professor) {
        try {
            int disciplinaCodigo = Integer.parseInt(disciplinaLine.split(" ")[0]);
            Disciplina disciplina = disciplinaRepository.findById(disciplinaCodigo)
                    .orElse(new Disciplina());
            disciplina.setCodigo(Integer.parseInt(disciplinaLine.split(" ")[0]));
            disciplina.setNome(disciplinaLine.split(" ")[1]);
            disciplina.setProfessor(professor);
            disciplinaRepository.save(disciplina);
            return disciplina;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private void saveAluno(String alunoLine, Disciplina disciplina) {
        try {
            int alunoMatricula = Integer.parseInt(alunoLine.split(" ")[0]);
            Aluno aluno = alunoRepository.findById(alunoMatricula)
                    .orElse(new Aluno());
            aluno.setMatricula(alunoMatricula);
            aluno.setNome(alunoLine.split(" ")[1]);

            Set<Disciplina> disciplinas = aluno.getDisciplinas();
            disciplinas.add(disciplina);
            aluno.setDisciplinas(disciplinas);

            alunoRepository.save(aluno);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}