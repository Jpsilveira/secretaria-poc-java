package br.com.jpsilveira.secretaria.controller;

import br.com.jpsilveira.secretaria.model.Professor;
import br.com.jpsilveira.secretaria.repository.ProfessorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class ProfessorController {

    private ProfessorRepository professorRepository;

    @Autowired
    public ProfessorController(ProfessorRepository repository) {
        this.professorRepository = repository;
    }

    @GetMapping("/professor/")
    public String listProfessores(Model model) {
        Iterable<Professor> professores = professorRepository.findAll();
        model.addAttribute("professores", professores);
        model.addAttribute("template", "professor");
        return "index";
    }

    @GetMapping("/professor/edit/{matricula}")
    public String editProfessor(@PathVariable("matricula") int matricula, Model model) {
        Professor professor = professorRepository.findById(matricula)
                .orElseThrow(() -> new IllegalArgumentException("Matrícula inválida:" + matricula));
        model.addAttribute("professor", professor);
        model.addAttribute("template", "update-professor");
        return "index";
    }

    @PostMapping("/professor/update/{matricula}")
    public String updateProfessor(@PathVariable("matricula") int matricula, @Valid Professor professor,
                                  BindingResult result, Model model) {
        if (result.hasErrors()) {
            professor.setMatricula(matricula);
            model.addAttribute("template", "update-professor");
            return "index";
        }

        professorRepository.save(professor);
        model.addAttribute("professores", professorRepository.findAll());
        model.addAttribute("template", "professor");
        return "index";
    }
}
