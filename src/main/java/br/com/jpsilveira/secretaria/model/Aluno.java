package br.com.jpsilveira.secretaria.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Aluno extends Pessoa {

    @ManyToMany(cascade = {
            CascadeType.PERSIST,
            CascadeType.MERGE
    })
    @JoinTable(
            name = "aluno_disciplina",
            joinColumns = @JoinColumn(name = "matricula_aluno"),
            inverseJoinColumns = @JoinColumn(name = "codigo_disciplina")
    )
    private Set<Disciplina> disciplinas = new HashSet<Disciplina>();

    public Set<Disciplina> getDisciplinas() {
        return disciplinas;
    }

    public void setDisciplinas(Set<Disciplina> disciplinas) {
        this.disciplinas = disciplinas;
    }
}