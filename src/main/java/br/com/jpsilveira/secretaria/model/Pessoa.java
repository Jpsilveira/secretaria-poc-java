package br.com.jpsilveira.secretaria.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.validation.constraints.NotBlank;
import java.util.Set;

@Entity
@Inheritance
public abstract class Pessoa {

    @Id
    private int matricula;

    @NotBlank(message = "O nome é obrigatório")
    private String nome;

    public int getMatricula() {
        return matricula;
    }

    public void setMatricula(int matricula) {
        this.matricula = matricula;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public abstract Set<Disciplina> getDisciplinas();
}
