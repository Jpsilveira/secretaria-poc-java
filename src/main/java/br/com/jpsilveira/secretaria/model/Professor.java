package br.com.jpsilveira.secretaria.model;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Professor extends Pessoa {

    @OneToMany(mappedBy = "professor")
    private Set<Disciplina> disciplinas = new HashSet<Disciplina>();

    @Override
    public Set<Disciplina> getDisciplinas() {
        return disciplinas;
    }

    public void setDisciplinas(Set<Disciplina> disciplinas) {
        this.disciplinas = disciplinas;
    }
}
