package br.com.jpsilveira.secretaria.repository;

import br.com.jpsilveira.secretaria.model.Aluno;
import br.com.jpsilveira.secretaria.model.Disciplina;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AlunoRepository extends CrudRepository<Aluno, Integer> {
    Optional<List<Disciplina>> findByDisciplinas_Codigo(int disciplinaCodigo);
}
