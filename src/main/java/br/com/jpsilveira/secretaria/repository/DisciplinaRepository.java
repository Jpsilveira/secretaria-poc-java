package br.com.jpsilveira.secretaria.repository;

import br.com.jpsilveira.secretaria.model.Disciplina;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DisciplinaRepository extends CrudRepository<Disciplina, Integer> {
    List<Disciplina> findAll(Sort sortOrder);
}
