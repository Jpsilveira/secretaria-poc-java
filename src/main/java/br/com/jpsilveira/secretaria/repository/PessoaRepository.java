package br.com.jpsilveira.secretaria.repository;

import br.com.jpsilveira.secretaria.model.Pessoa;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PessoaRepository extends CrudRepository<Pessoa, Integer> {
    List<Pessoa> findAll(Sort matricula);
}
