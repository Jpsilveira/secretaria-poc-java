package br.com.jpsilveira.secretaria.repository;

import br.com.jpsilveira.secretaria.model.Professor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProfessorRepository extends CrudRepository<Professor, Integer> {
}
